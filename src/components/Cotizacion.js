import React from 'react';
import styled from '@emotion/styled';

const ResultadoDiv = styled.div`
    color: #FFF;
    font-family: Arial, Helvetica, sans-serif;
`;

const Info = styled.p`
    font-size: 18px;
    span {
        font-weight:bold;
    }
`;
const Precio = styled.p`
    font-size: 30px;
    span {
        font-weight:bold;
    }
`

const Cotizacion = ({ resultado }) => {
    if(Object.keys(resultado).length === 0 ) return null;

    return (
        <ResultadoDiv>
            <Precio>El precio es: {resultado.PRICE}</Precio>
            <Info>Variación en 24h: {resultado.CHANGE24HOUR}</Info>
            <Info>Max valor 24h: {resultado.HIGH24HOUR}</Info>
            <Info>última marca: {resultado.LASTMARKET}</Info>
            <Info>Actualización: {resultado.LASTUPDATE}</Info>
        </ResultadoDiv>
    );

}

export default Cotizacion;