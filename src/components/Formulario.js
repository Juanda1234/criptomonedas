import React, { useEffect, useState } from 'react';
import styled from '@emotion/styled';

import Error from './Error'
import useMoneda from '../hooks/useMoneda';
import useCriptomoneda from '../hooks/useCriptomoneda';
import Axios from 'axios';

const Boton = styled.input`
    margin-top: 20px;
    font-weight: bold;
    font-size: 20px;
    padding: 10px;
    background-color: #66a2fe;
    border: none;
    width: 100%;
    border-radius: 10px;
    color: #fff;
    transition: background-color .4s ease;

    &:hover {
        background-color: #326AC0;
        cursor: pointer;
    }
`;

const Formulario = ({ guardarMoneda, guardarCriptomoneda }) => {

    // State del listado de criptomonedas

    const [ listaCripto, guardarCriptomonedas ] = useState([])

    // State control de error

    const [ error, guardarError ] = useState(false);

    const MONEDAS = [
        { codigo: 'USD', nombre: 'Dolar de Estados Unidos' },
        { codigo: 'MXN', nombre: 'Peso Mexicano' },
        { codigo: 'EUR', nombre: 'Euro' },
        { codigo: 'GBP', nombre: 'Libra Esterlina' }
    ];

    // Utilizar useMoneda

    const [ moneda, SelectMonedas ] = useMoneda('Elige tu Moneda', '', MONEDAS);

    // Utilizar useMoneda
    const [ criptoMoneda, SelectCripto ] = useCriptomoneda('Elige tu Criptomoneda', '', listaCripto);

    //ejecutar llamada a la api

    useEffect(() => {
        const consultarAPI = async () => {
            const url = 'https://min-api.cryptocompare.com/data/top/mktcapfull?limit=10&tsym=USD';

            const resultado = await Axios.get(url);
            
            guardarCriptomonedas(resultado.data.Data);

            console.log('resultado llamada cripto', resultado.data.Data)
        }

        consultarAPI();
    }, [])

    //cuando el usuario hace submit
    const cotizarMoneda = e => {
        e.preventDefault();

        //validar si ambos campos tienes contenido
        
        if(moneda.trim() === "" || criptoMoneda.trim() === "") {
            guardarError(true);
            return;
        }

        // pasar los datos al componente principal
        guardarMoneda(moneda)
        guardarCriptomoneda(criptoMoneda)
        guardarError(false);
    }

    return (
        <form
         onSubmit={cotizarMoneda}
        >
            {error ? <Error mensaje="Todos los campos son obligatorios"/>: null}
            <SelectMonedas/>
            <SelectCripto/>
            <Boton
                type="submit"
                value="Calcular"
            />
        </form>
    );
}

export default Formulario;